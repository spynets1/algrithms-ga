// Authers: Alfred Roos, Jennifer Taylor, Rahaf Jamoo
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "utils.h"

typedef unsigned int* Array;


void Merge(Array a, Array b, Array c, int n1,int n2){
    unsigned int i=0, j=0, index = 0;
    while(i < n1 || j < n2){
        if(j>=n2 || (i < n1 && b[i] <= c[j])){
            a[index] = b[i];
            i++;
            index++;
        }
        else if(j < n2){
            /* printf("Add %d\n",c[j]); */
            a[index] = c[j];
            j++;
            index++;
        }
    }
}
void split_half(Array a, Array b, Array c, int n){

    memcpy(b,a,sizeof(int)*n/2);
    memcpy(c,a+(n/2),sizeof(int)*n-(n/2));
}

Array MergeSort (Array arr,int n){
    if(n > 1){
        Array b;
        Array c;
        b = malloc(sizeof(int)*n/2);
        c = malloc(sizeof(int)*n-(n/2));

        split_half(arr,b,c,n);

        MergeSort(b,n/2);
        MergeSort(c,n-(n/2));
        Merge(arr,b,c,n/2,n-(n/2));

    }
    return arr;
}
