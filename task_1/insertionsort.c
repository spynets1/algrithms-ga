// Authers: Alfred Roos, Jennifer Taylor, Rahaf Jamoo

typedef unsigned int* Array;


Array InsertionSort(Array arr, int n){
    for(int i = 0; i < n; i++){

      unsigned int v = arr[i];
      int j = i-1;

      while( j >= 0 && arr[j] > v){
          arr[j+1] = arr[j];
          j = j-1;
      }
      arr[j+1] = v;
    }
    return arr;
}
