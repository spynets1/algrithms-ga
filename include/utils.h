// Authers: Alfred Roos, Jennifer Taylor, Rahaf Jamoo
#ifndef UTILS_H_
#define UTILS_H_

typedef unsigned int* Array;
typedef Array (*SortFunc)(Array, int, int*);

/**Function to print an unsgined int array*/
void print_array(Array arr, int n);
/**returns if an unsigned int array is sorted*/
char check(Array arr, int n);
/**
 * This function runs and algorithm and creates an output
 *
 * @param1 the name of the algorithm
 * @param2 The Algorithm passed in
 * */
void run_algorithm(char* algoritm_name,Array (*SortFunc)(Array,int, int*));

#endif //
