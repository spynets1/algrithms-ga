// Authers: Alfred Roos, Jennifer Taylor, Rahaf Jamoo
#include "utils.h"
#ifndef SORTING_H_
#define SORTING_H_


Array BubbleSort    (Array arr, int n, int* op);
Array InsertionSort (Array arr, int n, int* op);
Array MergeSort     (Array arr, int n, int* op);


#endif // SORTING_H_
