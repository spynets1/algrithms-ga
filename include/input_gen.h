// Authers: Alfred Roos, Jennifer Taylor, Rahaf Jamoo
#include "utils.h"

#ifndef INPUT_GEN_H_
#define INPUT_GEN_H_

#define ORDERED    0
#define REVERSE    1
#define RANDOM     2
#define SEMIRANDOM 3


/**
 * This function will return an unsigned int array with size n (provided as argument) with different order specified by the type argument
 * 0     - orderd array
 * 1     - reverse orderd
 * 2     - randomized
 * 3     - almost ordered
 * other - NULL
 *  */
Array get_array    (int n,int type);


#endif // INPUT_GEN_H_
