##
# Group assignment 1
# @file
# @version 0.1

cc = gcc -Wall -lm
sources =  ./src/sorting.c ./src/utils.c ./src/input_gen.c

mains = ./src/main.c
testings = ./src/testing.c

main = ./main
testing = ./testing

all:  $(sources)
	$(cc) $(sources) $(mains) -lm -o $(main)
	$(cc) $(sources) $(testings) -lm -o $(testing)

main:
	$(cc) $(mains) $(sources) -lm -o $(main)

testing:
	$(cc) $(sources) $(testings) -lm -o $(testing)

clean:
	rm -rf $(main) $(testing)

# end
