import numpy as np
import matplotlib.pyplot as plt

# Define the range of n values
n = np.arange(1, 2**16)

# Define the functions
func1 = (n - 1) * n / 2
func2 = n - 1
func3 = n ** 2 / 4
func4 = n * np.log2(n)

# Data for Merge sort
merge_ordered = {
    256: 2048,
    512: 4608,
    1024: 10240,
    2048: 22528,
    4096: 49152,
    8192: 106496,
    16384: 229376,
    32768: 491520
}

merge_reverse = {
    256: 2048,
    512: 4608,
    1024: 10240,
    2048: 22528,
    4096: 49152,
    8192: 106496,
    16384: 229376,
    32768: 491520
}

merge_random = {
    256: 2048,
    512: 4608,
    1024: 10240,
    2048: 22528,
    4096: 49152,
    8192: 106496,
    16384: 229376,
    32768: 491520
}

merge_almost_ordered = {
    256: 2048,
    512: 4608,
    1024: 10240,
    2048: 22528,
    4096: 49152,
    8192: 106496,
    16384: 229376,
    32768: 491520
}

# Data for Bubble sort
bubble_ordered = {
    256: 32640,
    512: 130816,
    1024: 523776,
    2048: 2096128,
    4096: 8386560,
    8192: 33550336,
    16384: 134209536,
    32768: 536854528
}

bubble_reverse = {
    256: 32640,
    512: 130816,
    1024: 523776,
    2048: 2096128,
    4096: 8386560,
    8192: 33550336,
    16384: 134209536,
    32768: 536854528
}

bubble_random = {
    256: 32640,
    512: 130816,
    1024: 523776,
    2048: 2096128,
    4096: 8386560,
    8192: 33550336,
    16384: 134209536,
    32768: 536854528
}

bubble_almost_ordered = {
    256: 32640,
    512: 130816,
    1024: 523776,
    2048: 2096128,
    4096: 8386560,
    8192: 33550336,
    16384: 134209536,
    32768: 536854528
}

# Data for Insertion sort
insertion_ordered = {
    256: 255,
    512: 511,
    1024: 1023,
    2048: 2047,
    4096: 4095,
    8192: 8191,
    16384: 16383,
    32768: 32767
}

insertion_reverse = {
    256: 32640,
    512: 130816,
    1024: 523776,
    2048: 2096128,
    4096: 8386560,
    8192: 33550336,
    16384: 134209536,
    32768: 536854528
}

insertion_random = {
    256: 17546,
    512: 69788,
    1024: 265395,
    2048: 1056244,
    4096: 4197209,
    8192: 16707218,
    16384: 67097103,
    32768: 268610723
}

insertion_almost_ordered = {
    256: 1278,
    512: 4161,
    1024: 17068,
    2048: 84054,
    4096: 357148,
    8192: 1321091,
    16384: 5187147,
    32768: 20794267
}

# Plotting Merge sort
plt.figure(figsize=(10, 6))
plt.plot(list(merge_ordered.keys()), list(merge_ordered.values()), label='Merge sort (Ordered)')
plt.plot(list(merge_reverse.keys()), list(merge_reverse.values()), label='Merge sort (Reverse ordered)')
plt.plot(list(merge_random.keys()), list(merge_random.values()), label='Merge sort (Random)')
plt.plot(list(merge_almost_ordered.keys()), list(merge_almost_ordered.values()), label='Merge sort (Almost ordered)')
plt.title('Merge Sort')
plt.xlabel('Size n')
plt.ylabel('Operations op')
plt.plot(n, func4, label='n*log2(n)')
plt.legend()
plt.grid(True)
plt.show()

# Plotting Bubble sort
plt.figure(figsize=(10, 6))
plt.plot(list(bubble_ordered.keys()), list(bubble_ordered.values()), label='Bubble sort (Ordered)')
plt.plot(list(bubble_reverse.keys()), list(bubble_reverse.values()), label='Bubble sort (Reverse ordered)')
plt.plot(list(bubble_random.keys()), list(bubble_random.values()), label='Bubble sort (Random)')
plt.plot(list(bubble_almost_ordered.keys()), list(bubble_almost_ordered.values()), label='Bubble sort (Almost ordered)')
plt.title('Bubble Sort')
plt.xlabel('Size n')
plt.ylabel('Operations op')
plt.plot(n, func1, label='(n-1)*n/2')
plt.legend()
plt.grid(True)
plt.show()

# Plotting Insertion sort
plt.figure(figsize=(10, 6))
plt.plot(list(insertion_ordered.keys()), list(insertion_ordered.values()), label='Insertion sort (Ordered)')
plt.plot(list(insertion_reverse.keys()), list(insertion_reverse.values()), label='Insertion sort (Reverse ordered)')
plt.plot(list(insertion_random.keys()), list(insertion_random.values()), label='Insertion sort (Random)')
plt.plot(list(insertion_almost_ordered.keys()), list(insertion_almost_ordered.values()), label='Insertion sort (Almost ordered)')
plt.title('Insertion Sort')
plt.xlabel('Size n')
plt.ylabel('Operations op')
plt.plot(n, func1, label='(n-1)*n/2')
plt.plot(n, func2, label='(n-1)')
plt.plot(n, func3, label='n^2/4')
plt.legend()
plt.grid(True)
plt.show()
