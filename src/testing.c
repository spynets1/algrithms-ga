// Authers: Alfred Roos, Jennifer Taylor, Rahaf Jomaa
/**
 * This is one to let a user interact and test the algorithms on their own
 * */

#include <stdio.h>
#include "../include/sorting.h"

int main(){

    unsigned int ans = 0;
    while(ans < 4){
        puts("Which algorithm do you want to test?");
        puts("1: Bubble sort");
        puts("2: Insertion sort");
        puts("3: Merge sort");
        puts("4: quit");
        scanf("%d",&ans);

        switch(ans){
            case 1:
                run_algorithm("Bubble sort",BubbleSort);
                break;
            case 2:
                run_algorithm("Insertion sort",InsertionSort);
                break;
            case 3:
                run_algorithm("Merge sort",MergeSort);
                break;
        }
    }


    return 0;
}
