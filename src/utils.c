// Authers: Alfred Roos, Jennifer Taylor, Rahaf Jomaa
/**
 * This file is has some utils used in other parts of the program
 * */
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#include "../include/utils.h"
#include "../include/input_gen.h"


void print_array(Array arr, int n){
    for(int i = 0; i < n; i ++){
        printf("%d,",arr[i]);
    }
    puts("");
}

char check(Array arr, int n){
    int largest = INT_MAX;
    for(int i = n-2; i >= 0; i --) {
        if(arr[i] > largest){
            return 'F';
        }
        largest = arr[i];
    }
    return 'T';
}

void run_algorithm(char* algoritm_name, Array (*SortFunc)(Array,int, int*)){

    for(int i = 0 ; i <= 3; i ++){
        //print witch algorithm we are testing
        printf("Algorithm: %s\n",algoritm_name);
        switch(i){
            case 0:
                printf("Input:  Ordered input\n");
                break;
            case 1:
                printf("Input: Reverse ordered input\n");
                break;
            case 2:
                printf("Input: Random input\n");
                break;
            case 3:
                printf("Input: almost ordered input\n");
                break;
        }
        printf("Size n     Operations op\n");
        puts(  "------------------------------");

        for(int j = 8; j < 16; j ++){

            int n = (int)pow(2,j);
            int op = 0;

            // retrive an array
            Array arr = get_array(n,i);
            //we should run theise 30 times
            if(i > 1){
                long sum = 0;
                for(int l = 0; l < 30; l ++){
                    SortFunc(arr,n,&op);
                    arr = get_array(n,i);
                    sum += op;
                    op = 0;
                }
                op = sum/30;
            }
            else{
                SortFunc(arr,n,&op);
            }

            //  format printing line
            char number[14];
            sprintf(number,"%d",n);
            printf("%d",n);
            for(int k = 0 ; k < 15 - strlen(number); k ++){
                printf(" ");
            }
            printf("%ud\n",op);

            free(arr);
        }
        puts("");
    }
}
