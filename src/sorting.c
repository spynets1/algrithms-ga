// Authers: Alfred Roos, Jennifer Taylor, Rahaf Jomaa
#include "../include/utils.h"

#include <stdbool.h>
#include <string.h>
#include <stdlib.h>

/*Bubble sort*/
Array BubbleSort(Array arr, int n, int *op) {
    for (int i = 0; i < n - 1; i++) {
        for (int j = 0; j < n - i - 1; j++) {
            (*op)+=1;
            if (arr[j] > arr[j + 1]) {
                int temp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = temp;
            }
        }
    }
    return arr;
}

/*Merge sort*/
void Merge(Array a, Array b, Array c, int n1,int n2, int* op){
    unsigned int i=0, j=0, index = 0;
    while(i < n1 || j < n2){
        if(j>=n2 || (i < n1 && b[i] <= c[j])){
            a[index] = b[i];
            i++;
            index++;
        }
        else if(j < n2){
            /* printf("Add %d\n",c[j]); */
            a[index] = c[j];
            j++;
            index++;
        }
        (*op)++;
    }
}
void split_half(Array a, Array b, Array c, int n){

    memcpy(b,a,sizeof(int)*n/2);
    memcpy(c,a+(n/2),sizeof(int)*n-(n/2));
}
Array MergeSort (Array arr,int n, int *op){
    if(n > 1){
        Array b;
        Array c;
        b = malloc(sizeof(int)*n/2);
        c = malloc(sizeof(int)*n-(n/2));

        split_half(arr,b,c,n);

        MergeSort(b,n/2,op);
        MergeSort(c,n-(n/2),op);
        Merge(arr,b,c,n/2,n-(n/2),op);

    }
    return arr;
}

/*Insertion sort*/
Array InsertionSort(Array arr, int n, int* op){
    for(int i = 0; i < n-1; i++){

        unsigned int v = arr[i];
        int j = i-1;

        (*op)++;
        while( j >= 0 && arr[j] > v){
            arr[j+1] = arr[j];
            j = j-1;
            (*op)++;
        }
        arr[j+1] = v;
    }
    return arr;
}
