// Authers: Alfred Roos, Jennifer Taylor, Rahaf Jomaa
/**
 * This program will test all algorithms and spit the result out to stdout
 * */
#include "../include/input_gen.h"
#include "../include/utils.h"
#include "../include/sorting.h"

int main(){
    /* print_array(get_array(100, 3), 99); */
    run_algorithm("Merge sort",MergeSort);
    run_algorithm("Bubble sort",BubbleSort);
    run_algorithm("Insertion sort",InsertionSort);
    return 0;
}

