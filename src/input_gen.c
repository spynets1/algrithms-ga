// Authers: Alfred Roos, Jennifer Taylor, Rahaf Jomaa

#include <stdlib.h>
#include <time.h>
#include <stdio.h>

#include "../include/input_gen.h"
#include "../include/utils.h"

void swap(unsigned int *a, unsigned int *b) {
    int temp = *a;
    *a = *b;
    *b = temp;
}

Array get_array (int n,int type){
    Array arr = malloc(sizeof(int)*n);
    srand(time(NULL));
    switch(type){
        case ORDERED:
            for(int i = 0; i < n; i ++){
                arr[i] = i;
            }
            return arr;
        case REVERSE:
            for(int i = 0; i < n; i ++){
                arr[i] = n-i;
            }
            return arr;
        case RANDOM:
            for(int i = 0; i < n; i ++){
                arr[i] = i;
            }
            for(int i = 0; i < n; i ++){
                int j = rand() % (i + 1);
                //swap
                unsigned int temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
            }
            return arr;
        case SEMIRANDOM:
            for(int i = 0; i < n; i ++){
                arr[i] = i;
            }
            for(int i = 0; i < n; i ++){
                if ((double)rand() / RAND_MAX < 0.04) {
                    int j = rand() % (i + 1);
                    //swap
                    unsigned int temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
                }
            }
            return arr;

    }
    free(arr);
    return NULL;
}
